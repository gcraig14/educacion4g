$(function(){

//  Animate logo

	var introTL = new TimelineMax({
			delay:1
		}); 

	introTL.from($('.intro-img'),1,{x:-2000},0)
		  .from($('.main-description'),1,{x:3000},0)
		  .from($('#intro h4'),1,{y:-900, autoAlpha: 0},0.5)
		  .from($('#intro h1'),1,{x:-2900, autoAlpha: 0},'-=0.3')
		  .from($('#intro h1 span'),1,{x:2900, autoAlpha: 0},'-=0.8')
		  .from($('#intro p'),1,{scale:0, autoAlpha: 0},'+=0.1')
		  .from($('#intro a'),1,{scale:0, autoAlpha: 0},'-=0.1');

//  Animate Inicio


	var cont01 = new ScrollMagic.Controller({
		globalSceneOptions: {triggerHook: "onEnter", duration: "200%"}
	});

	var inicio = new ScrollMagic.Scene({
	    triggerElement: '#inicio',  // what will trigger scene
	    reverse: true,
	}).setTween("#canvas", {y: "80%", ease: Linear.easeNone})
	  .addTo(cont01);

//  Animate About	

	var controller = new ScrollMagic.Controller();

	var tween = new TimelineMax().from($('#byod h2'),0.9,{scale:0, autoAlpha: 0},0)
								 .from($('#byod h3'),0.8,{scale:0, autoAlpha: 0},0.3)
								 .from($('#byod p'),0.8,{x:-1500, autoAlpha: 0},0.6)
								 .from($('#byod img'),0.8,{x:1500, autoAlpha: 0},0.6);

	var byod = new ScrollMagic.Scene({
	    duration: 0, // duration in px eg. 300, 0 = autoplay
	    offset: 0, // offset trigger position by 100px
	    triggerElement: '#byod',  // what will trigger scene
	    triggerHook: 1, // 0=top, 0.5=middle, 1=bottom
	    reverse: true,
	}).setTween(tween)
	  .addTo(controller);

//  Animate Works

	var controllerWorks = new ScrollMagic.Controller();

	var tweenWorksBG = new TimelineMax().from($('.triang-bg .divanim1'),1,{y:600,x:2000,rotation:-45, autoAlpha: 0},0)
										.from($('.triang-bg .divanim2'),1,{y:600,x:-2000,rotation:45, autoAlpha: 0},0.5)
										.from($('.triang-bg .divanim3'),1,{y:600,x:-2000,rotation:45, autoAlpha: 0,},1)
										.from($('.triang-bg .divanim4'),1,{y:600,x:2000,rotation:-45, autoAlpha: 0,},1.1)
										.from($('.triang-bg .divanim5'),1,{y:600,x:-2000,rotation:45, autoAlpha: 0,},1.4)
										.from($('.triang-bg .divanim6'),1,{y:600,x:2000,rotation:-45, autoAlpha: 0,},1.5)
										.from($('.triang-bg .divanim7'),1,{y:600,x:2000,rotation:-45, autoAlpha: 0,},1.7);

	var works = new ScrollMagic.Scene({
	    duration: 0, // duration in px eg. 300, 0 = autoplay
	    triggerElement: '#works',  // what will trigger scene
	    triggerHook: 0.5, // 0=top, 0.5=middle, 1=bottom
	    reverse: true,
	}).setTween(tweenWorksBG)
	  .addTo(controllerWorks);
	  
});
